<?php
/**
 * Компонент :
 */

namespace app\components;


class Formatter
{
    /**
     * @param $number
     * @param string $mask
     * @return string
     *
     * $mask м.б.в формате "+9 (999) 999-99-99" или любой другой
     * Цифры "9" заменяются реальным данными из $number
     */
    public static function phone(int $number, string $mask = '') : string
    {
        $snumber = (string) $number;
        if (empty($mask)) {
            return $snumber;
        }

        $numLength = strlen($snumber);
        $maskLength = strlen($mask);
        $result = '';
        $n = 0;

        for ($m = 0; $m < $maskLength; $m++) {
            $symbol = ($mask[$m] == '9') ? $snumber[$n++] : $mask[$m];
            $result .= $symbol;

            if ($n == $numLength) {
                break;
            }
        }

        if ($n < $numLength) {
            $result .= substr($snumber, $n);
        }

        return $result;
    }
}


/**
 * Контроллер :
 */
    public function actionFormat()
    {
        echo Formatter::phone(12345678900) . '<br />';
        echo Formatter::phone(12345678900, '+9 (999) 999-99-99') . '<br />';
        echo Formatter::phone(12345678900, '+9 999 999-9999') . '<br />';
        echo Formatter::phone(12345678900, '9 999 999 99 99') . '<br />';
    }
