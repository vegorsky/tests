<?php
/**
 * Модель :
 */

namespace app\components;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class ProductSearchModel extends Model
{
    public $id;
    public $categoryId;
    public $price;
    public $hidden;

    public function rules()
    {
        return [
            [['id', 'categoryId', 'price', 'hidden'], 'integer'],
        ];
    }

    public function search()
    {
        $xml = simplexml_load_file(\Yii::getAlias('@webroot/files/products.xml'));
        $json = json_encode($xml);
        $data = json_decode($json,TRUE);
        $data = $data['item'];

        $filters = Yii::$app->request->get('ProductSearchModel');
        $allModels = array_filter($data, function($model) use ($filters) {
            foreach ($filters as $key => $value) {
                if ($value !== '' && $model[$key] !== $value) {
                    return false;
                }
            }

            return true;
        });
        $this->setAttributes($filters);

        return new ArrayDataProvider([
            'allModels' => $allModels,
            'key'=>'id',
            'sort' => [
                'attributes' => ['id', 'categoryId', 'price', 'hidden',],
            ],
        ]);
    }
}


    /**
     * Контроллер :
     * 
     * @return string
     */
    public function actionProducts()
    {
        $searchModel = new ProductSearchModel();
        $productDataProvider = $searchModel->search();

        $xml = simplexml_load_file(Yii::getAlias('@webroot/files/categories.xml'));
        $json = json_encode($xml);
        $data = json_decode($json,TRUE);
        $categoryListData = [];
        foreach($data['item'] as $item) {
            $categoryListData[$item['id']] = $item['name'];
        }

        return $this->render('products', [
            'productDataProvider' => $productDataProvider,
            'categoryListData' => $categoryListData,
            'searchModel' => $searchModel,
        ]);
    }



<?php
/**
 * Вьюха :
 * 
 * @var $productDataProvider yii\data\BaseDataProvider
 * @var $categoryListData array
 * @var $searchModel app\components\ProductSearchModel
 */

use yii\grid\GridView;
?>

<div class="site-products">
    <?= GridView::widget([
        'id' => 'grid_products',
        'dataProvider' => $productDataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'categoryId',
                'format' => 'raw',
                'value' => function ($model) use ($categoryListData) {
                    return $categoryListData[$model['categoryId']];
                },
                'filter' => $categoryListData,
            ],
            'price',
            'hidden',
        ],
    ]); ?>
</div>
